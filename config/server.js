/* importar o módulo do framework express */
const express = require('express')
const path = require('path')

/* importar o módulo do consign */
var consign = require('consign');

var app = express()
  .use(express.static(path.join(__dirname, '../www/public')))
  .set('views', path.join(__dirname, '../www/views'))
  .set('view engine', 'ejs')

/* efetua o autoload das rotas, dos models e dos controllers para o objeto app */
consign({cwd: 'www'})
    .include('routes')
    .then('controllers')
	.into(app);

/* exportar o objeto app */
module.exports = app;