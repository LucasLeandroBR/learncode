const jwt = require('jsonwebtoken')

const authtoken = process.env.JWT_SECRET;

module.exports = (req, res, next) => {
    const authHeader = req.headers.authorization;

    //Check if token is provided
    if(!authHeader) return res.status(401).send({error: 'No token provided'})

    const parts = authHeader.split(' ');

    //Check if token has to parts
    if(parts.length !== 2) return res.status(401).send({error: 'Token error'})

    const [ scheme, token ] = parts
    
    //Check if start with Bearer
    if(!/^Bearer$/i.test(scheme)) return res.status(401).send({error: 'Token malformatted'})

    jwt.verify(token, authtoken, (err, decoded) =>{
        if(err) return res.status(401).send({error: 'Token invalid'})
        req.userId = decoded.id

        return next()
    })
}