const express = require('express')
const bcrypt = require('bcrypt')
const crypto = require('crypto')

const mailer = require('../utils/mail')

const User = require('../models/User')
const Profile = require('../models/Profile')

const router = express.Router()

const AuthUtils = require('../utils/auth')
const Message = require('../utils/messages')

router.post('/register', async (req, res) => {
    const { name, email, password, confirm_password } = req.body

    try {
        //Check if email is in use
        if (await User.findOne({ email }))
            return Message.Error(res, 'Email already in use');

        //Password confirmation
        if (!confirm_password) return Message.Error(res, 'Confirm Password');
        if (confirm_password !== password) return Message.Error(res, 'Passwords doesnt match');

        //Create User on Database
        const users = await new User({ email, password });
        const profile = await new Profile({ name, assignedTo: users.id })
        await profile.save()
        users.profile = profile.id
        await users.save()

        let userRes = {users}
        
        userRes.users.password = undefined
        user = userRes.users

        //Return User's Informations
        return Message.Send(res, {
            user,
            profile,
            token: AuthUtils.generateToken({ id: user.id })
        });

    } catch (err) {
        //In case of error return Registration Failed
        console.log(err)
        return Message.Error(res, 'Registration Failed')
    }
})

router.post('/login', async (req, res) => {
    const { email, password } = req.body;

    const user = await User.findOne({ email }).select('+password').populate('profile');

    if (!user) return Message.Error(res, 'Authentication Failed');

    if (!await bcrypt.compare(password, user.password)) return Message.Error(res, 'Authentication Failed')

    user.password = undefined

    Message.Send(res, {
        user,
        token: AuthUtils.generateToken({ id: user.id })
    })
})

router.post('/forgot_password', async (req, res) => {
    const { email } = req.body

    try {
        const user = await User.findOne({ email })

        if (!user) return Message.Error(res, 'User not found');

        const token = crypto.randomBytes(20).toString('hex')

        const now = new Date();
        now.setHours(now.getHours() + 1);

        await User.findByIdAndUpdate(user.id, {
            '$set': {
                passwordResetToken: token,
                passwordResetExpires: now
            }
        })

        mailer.sendMail({
            to: email,
            from: 'joaof.renor@gmail.com',
            template: 'forgot_password',
            context: { token },
        }, (err) => {
            if (err) return Message.Error(res, 'Cannot send forgot password email');

            return Message.Success(res, 'Check your email box')
        })

    } catch (err) {

    }
})

router.post('/reset_password', async (req, res) => {
    const { email, token, password } = req.body;

    try {
        const user = await User.findOne({ email }).select('+passwordResetToken passwordResetExpires')

        if (!user) return Message.Error(res,'User not found' )

        if (token !== user.passwordResetToken) return Message.Error(res,'Token invalid' )

        const now = new Date()

        if (now > user.passwordResetExpires) return Message.Error(res, 'Your token expires, generete a new one' )

        user.password = password
        user.passwordResetToken = undefined
        user.passwordResetExpires = undefined

        await user.save()
        Message.Success(res, 'Password sucessfuly reseted')

    } catch (err) {

    }
})

module.exports = app => app.use('/auth', router);