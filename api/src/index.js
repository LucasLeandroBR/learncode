require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');

const app = express();

const port = process.env.PORT || 3333;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

require('./controllers')(app);


app.listen(port, () => console.log(`Server Running on PORT : ${port}`)) ;