const mongoose = require('../database');

const ProfileSchema = new mongoose.Schema({
    name: {
        type:String,
        required: true
    },
    role:{
        type: String,
        default: 'free'
    },
    status:{
        type: String,
        default: 'unverified'
    },
    assignedTo: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true,
        select: false
    },
    createdAt:{
        type: Date,
        default: Date.now()
    }
});

const Profile = mongoose.model('Profile', ProfileSchema);

module.exports = Profile;