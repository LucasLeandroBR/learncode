const jwt = require('jsonwebtoken')
const authtoken = process.env.JWT_SECRET;

module.exports = {
    generateToken: function(params = []) {
        return jwt.sign(params, authtoken, {
            expiresIn: '1m'
        })
    }
}