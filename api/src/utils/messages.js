module.exports = {
    Error:(res, msg) => res.status(400).send({ error: msg }),
    Success:(res, msg) => res.send({ok: msg}),
    Send: (res, msg) => res.send(msg),
}