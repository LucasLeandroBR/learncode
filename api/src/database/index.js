const mongoose = require('mongoose');
const mongo_uri = process.env.MONGO_URI;

//Connect on MongoDatabase 
mongoose.connect(mongo_uri, {useNewUrlParser: true})
.then(() => console.log('Database Connected'))
.catch((err) => console.log(e)); 

//Use Global Promise as Mongoose Promise
mongoose.Promise = global.Promise;

module.exports = mongoose